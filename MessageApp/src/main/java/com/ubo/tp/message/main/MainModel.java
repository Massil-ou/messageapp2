package main.java.com.ubo.tp.message.main;

import javax.swing.*;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class MainModel implements MainObservableController {
    private JPanel viewPanel;
    private List<MainObserver> observers = new ArrayList<>();

    public MainModel(JPanel viewPanel) {
        this.viewPanel = viewPanel;
    }

    public JPanel getViewPanel() {
        return viewPanel;
    }

    public void setViewPanel(JPanel viewPanel) {
        this.viewPanel = viewPanel;
        notifyObservers();
    }

    @Override
    public void addObserver(MainObserver observer) {
        observers.add(observer);
    }

    @Override
    public void notifyObservers() {
        for (MainObserver observer : observers) {
            observer.update();
        }
    }
}
