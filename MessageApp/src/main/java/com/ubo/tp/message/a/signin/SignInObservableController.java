package main.java.com.ubo.tp.message.a.signin;

public interface SignInObservableController {
    void addObserver(SignInObserver observer);
    void removeObserver(SignInObserver observer);
    void notifyObservers();
    void Signin();

}

interface SignInObserver {
    void Signin();
    void navigateToLoginView();

}
