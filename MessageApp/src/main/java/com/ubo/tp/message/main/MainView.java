package main.java.com.ubo.tp.message.main;


import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.File;

public class MainView extends JFrame {

    private MainModel model;

    public MainView(MainModel model) {
        super("Mock Application");
        this.model = model;
    }
    public void init() {

        Toolkit toolkit = Toolkit.getDefaultToolkit();
        Dimension screenSize = toolkit.getScreenSize();
        int screenWidth = screenSize.width;
        int screenHeight = screenSize.height;

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        add(this.model.getViewPanel());

        // Menu bar
        JMenuBar menuBar = new JMenuBar();

        // File menu
        JMenu fileMenu = new JMenu("Fichier");
        fileMenu.setMnemonic(KeyEvent.VK_F);

        // Exit item
        JMenuItem exitMenuItem = new JMenuItem("Quitter");
        exitMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });
        fileMenu.add(exitMenuItem);

        // Help menu
        JMenu helpMenu = new JMenu("Aide");
        helpMenu.setMnemonic(KeyEvent.VK_A);

        // About item
        JMenuItem aboutMenuItem = new JMenuItem("A propos");
        aboutMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(MainView.this,
                        "Ceci est une application de démonstration.",
                        "A propos",
                        JOptionPane.INFORMATION_MESSAGE);
            }
        });
        helpMenu.add(aboutMenuItem);

        menuBar.add(fileMenu);
        menuBar.add(helpMenu);
        setJMenuBar(menuBar);

        // File selector
        JButton fileChooserButton = new JButton("Sélectionner un répertoire");
        fileChooserButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser fileChooser = new JFileChooser();
                fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
                fileChooser.setDialogTitle("Sélectionner un répertoire");
                int result = fileChooser.showOpenDialog(MainView.this);
                if (result == JFileChooser.APPROVE_OPTION) {
                    File selectedFile = fileChooser.getSelectedFile();
                    System.out.println("Répertoire sélectionné : " + selectedFile.getAbsolutePath());
                }
            }
        });


        // Adjust the size of the window
        int desiredWidth = screenWidth / 2;
        int desiredHeight = screenHeight / 2;
        setSize(desiredWidth, desiredHeight);
        setLocationRelativeTo(null);

        // Set Look&Feel
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
            SwingUtilities.updateComponentTreeUI(this);
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex) {
            ex.printStackTrace();
        }


    }





}
