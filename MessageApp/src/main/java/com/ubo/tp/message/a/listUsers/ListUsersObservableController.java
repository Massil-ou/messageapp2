package main.java.com.ubo.tp.message.a.listUsers;


public interface ListUsersObservableController {

    void addObserver(ListUsersObserver observer);
    void removeObserver(ListUsersObserver observer);
    void notifyObservers();
    void update();
    void supp();


}

interface ListUsersObserver {
    void navigateToSigninView();
    void update();
    void supp();


}
