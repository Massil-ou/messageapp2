package main.java.com.ubo.tp.message.main;

public class MainController implements MainObserver {
    private MainModel model;
    private MainView view;

    public MainController(MainModel model, MainView view) {
        this.model = model;
        this.view = view;
        this.model.addObserver(this);
    }

    public void init() {
        this.view.init();
        view.setVisible(true);
    }

    @Override
    public void update() {
        // Ajoutez ici la logique de mise à jour de la vue en fonction des changements dans le modèle
    }
}
