package main.java.com.ubo.tp.message.a;

import main.java.com.ubo.tp.message.datamodel.User;

import java.util.Observable;


public interface MainObserver {
    void update(Observable o, Object arg);

    void update(Observable o, Object arg, User user);

}
