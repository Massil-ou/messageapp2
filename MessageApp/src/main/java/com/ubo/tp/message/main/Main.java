//package main.java.com.ubo.tp.message.main;
//
//import main.java.com.ubo.tp.message.exemple.LoginModel;
//import main.java.com.ubo.tp.message.exemple.LoginPanel;
//
//import javax.swing.*;
//
//public class Main {
//    public static void main(String[] args) {
//        SwingUtilities.invokeLater(new Runnable() {
//            @Override
//            public void run() {
//                LoginModel loginModel = new LoginModel("","");
//                JPanel viewPanel = new LoginPanel(loginModel); // Vous devez implémenter LoginPanel
//                MainModel model = new MainModel(viewPanel);
//                MainView view = new MainView(model);
//                MainController controller = new MainController(model, view);
//                controller.init();
//            }
//        });
//    }
//}
