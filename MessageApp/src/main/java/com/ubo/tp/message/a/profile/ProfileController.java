package main.java.com.ubo.tp.message.a.profile;

import main.java.com.ubo.tp.message.a.NavigationModel;
import main.java.com.ubo.tp.message.core.EntityManager;
import main.java.com.ubo.tp.message.core.database.IDatabase;

public class ProfileController implements ProfileObserver {
    private NavigationModel model;
    private ProfileModel loginmodel;
    private ProfileView profileView;
    protected IDatabase mDatabase;

    protected EntityManager mEntityManager;

    public ProfileController(NavigationModel model, ProfileModel loginmodel, IDatabase database, EntityManager entityManager) {
        this.model = model;
        this.loginmodel=loginmodel;
        this.mDatabase = database;
        this.mEntityManager = entityManager;
        this.loginmodel.addObserver(this);
    }
    public void setView(ProfileView profileView){
        this.profileView=profileView;
    }

    public ProfileView getView(){
        return profileView;
    }

    @Override
    public void navigateToSigninView() {
        model.navigateTo("SignInView");
    }

}